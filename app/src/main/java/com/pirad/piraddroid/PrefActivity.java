package com.pirad.piraddroid;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;

public class PrefActivity extends PreferenceActivity{
	 
	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.prefs);

        // shared preferences
        SharedPreferences prefs = getPreferenceScreen().getSharedPreferences();

        // setup ip address preference
        Preference ipPref = getPreferenceScreen().findPreference("addr");
        ipPref.setSummary(prefs.getString("addr","<none>"));
        ipPref.setOnPreferenceChangeListener(prefChangeListener);

        // setup port preference
        Preference portPref = getPreferenceScreen().findPreference("port");
        portPref.setSummary(prefs.getString("port","<none>"));
        portPref.setOnPreferenceChangeListener(prefChangeListener);
	}


    Preference.OnPreferenceChangeListener prefChangeListener = new Preference.OnPreferenceChangeListener() {
        public boolean onPreferenceChange(Preference preference, Object newValue) {
            // set summary value for relevant pref to the updated pref value
            preference.setSummary(newValue.toString());
            return true;
        }
    };


}