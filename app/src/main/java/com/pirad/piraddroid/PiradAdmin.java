package com.pirad.piraddroid;

/*
 * PiradAdmin
 *  class for interfacing to server
 *  provides standard commands for the server to respond
 * 
 */
public class PiradAdmin {

	private NetClient _netClient;	// actual connection class
	private String _addr;
	private int _port;
	
	public PiradAdmin(String addr, int port)
	{
		this._port = port;
		this._addr = addr;
	}
	
	public boolean canConnect()
	{
		this._netClient = new NetClient(this._addr, this._port);		
		return this._netClient.canConnect();
	}
	
	
	public String stop()
	{
		if(this.canConnect()) {
			this._netClient = new NetClient(this._addr, this._port);		
			this._netClient.sendDataWithString("STOP");
			return this._netClient.receiveDataFromServer();
		}
		else return "";
	}
	
	public String play()
	{
		if(this.canConnect()) {
			this._netClient = new NetClient(this._addr, this._port);		
			this._netClient.sendDataWithString("PLAY");
			return this._netClient.receiveDataFromServer();			
		}
		else return "";
	}

	public String list()
	{
		if(this.canConnect()) {
			this._netClient = new NetClient(this._addr, this._port);		
			this._netClient.sendDataWithString("LIST");
			return this._netClient.receiveDataFromServer();			
		}
		else return "";
	}

	public String current()
	{
		if(this.canConnect()) {
			String retVal;
			this._netClient = new NetClient(this._addr, this._port);		
			this._netClient.sendDataWithString("INFO");
			retVal = this._netClient.receiveDataFromServer();
			if (retVal.equals("")) {
				return "Nothing, stopped";
			}
			else return retVal;
		}
		else return "";
	}

	public String select(int id)
	{
		if(this.canConnect()) {
			this._netClient = new NetClient(this._addr, this._port);		
			this._netClient.sendDataWithString("SEL " + Integer.toString(id));
			return this._netClient.receiveDataFromServer();
		}
		else return "";
	}

	
}
