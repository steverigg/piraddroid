package com.pirad.piraddroid;

import java.util.concurrent.ExecutionException;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/*
 * main activity 
 */
public class MainActivity extends ListActivity 
{

	private PiradAdmin admin;	// admin class for network interface
	private String ip;			// ip address of pirad server
	private int port;			// port of pirad server
	
	private Handler mHandler;	// handler for polling for stream updates
	private static final int mInterval = 30000;	// polling interval for reading player info

	
	
	
/*****************************************************************************
 * 
 *	UI handlers
 * 
 *****************************************************************************/		

	/*
	 * Stop button pressed
	 * 	check connection, then execute async task and update stream info
	 */
	public void onStopClick(View v)	{
		if(this.canConnect()) {
			new cmdStopTask().execute();
			getCurrentStreamInfo();
		}
	}

	/*
	 * Play button pressed
	 * 	check connection, then execute async task and update stream info
	 */
	public void onPlayClick(View v)	{
		if(this.canConnect()) {
			new cmdPlayTask().execute();
			getCurrentStreamInfo();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
				
		this.initialise();	// initialisation code
	}

	/*
	 * (non-Javadoc)
	 * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	/**
	 * Options menu item selected
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle item selection
	    switch (item.getItemId()) {
	        case R.id.action_settings:
	            openSettings();
	            return true;
	        case R.id.action_reconnect:
	            this.initialise();
	            return true;
	        case R.id.action_about:
	            this.doAbout();
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}	
	
	
	/**
	 * initialisation
	 */
	private void initialise() {
		// get default prefs (if first load)
		//  then get user prefs
		PreferenceManager.setDefaultValues(this, R.xml.prefs, false);
		this.loadPreferences();

		// create new admin class
		admin = new PiradAdmin(this.ip, this.port);
		
		// if class created and can connect, 
		//	get stream info, set title and start info poll
		if(this.canConnect()) {
			this.getStreams();
			String newTitle = "pirad on ";
			newTitle += this.ip + ":" + Integer.toString(this.port);
			this.setTitle(newTitle);

			this.mHandler = new Handler();
			this.startRepeatingTask();
		}
		
		// otherwise, set label to indicate connection failed
		else {
			TextView txtNowPlaying = (TextView) findViewById(R.id.txtNowPlaying);
	    	txtNowPlaying.setText(R.string.sConnectionFailed);			
			this.setTitle(R.string.app_name);
		}
	}
	
	/*
	 * get current stream information
	 */
	private void getCurrentStreamInfo() {
		if(this.canConnect()) {
			new cmdGetInfoTask().execute();
		}
	}

	/*
	 * check connection to server 
	 */
	private boolean canConnect() {
		boolean retVal;
		try {
			retVal = new cmdCheckNetworkTask().execute().get();
		 
		}
		catch (Exception e) {
			retVal = false;
		}
		return retVal;
	}
	
	/*
	 * get list of available streams (playlist) from server 
	 */
	private void getStreams() {
		
		String fullList;	// return value from server
		try {
			fullList = new cmdGetStreamsTask().execute().get();

			// split using ; as token
			String[] theList = fullList.split(";");
			
			// if we have items, build the list view and assign click event
			if (theList.length > 0) {
				
				// build the list view and adapter
				ListView lvwStreamList = getListView();
				lvwStreamList.setTextFilterEnabled(true);
				ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.list_streams, theList);
				lvwStreamList.setAdapter(adapter);

				// assign click handler
				lvwStreamList.setOnItemClickListener(new OnItemClickListener() {
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {
					    				
						// when clicked, play index + 1 and update stream info
						new cmdPlaySelectionTask().execute(position + 1);
						getCurrentStreamInfo();
						
						// When clicked, show a toast with the TextView text												
					    Toast.makeText(getApplicationContext(),
					    		((TextView) view).getText(), Toast.LENGTH_SHORT).show();
					}
				});			
			}
						
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
	}
			

	
/*****************************************************************************
 * 
 *	AsyncTasks
 * 
 *****************************************************************************/		
	
	private class cmdCheckNetworkTask extends AsyncTask<String, Void, Boolean> {
	    /** The system calls this to perform work in a worker thread and
	      * delivers it the parameters given to AsyncTask.execute() */
	    protected Boolean doInBackground(String... actions) {
	    	return admin.canConnect();
	    }	    
	}		
	
	private class cmdStopTask extends AsyncTask<String, Void, String> {
	    /** The system calls this to perform work in a worker thread and
	      * delivers it the parameters given to AsyncTask.execute() */
	    protected String doInBackground(String... actions) {
	    	return admin.stop();
	    }
	}		
	
	private class cmdPlayTask extends AsyncTask<String, Void, String> {
	    /** The system calls this to perform work in a worker thread and
	      * delivers it the parameters given to AsyncTask.execute() */
	    protected String doInBackground(String... actions) {
	    	return admin.play();
	    }
	}		

	private class cmdPlaySelectionTask extends AsyncTask<Integer, Void, String> {
	    /** The system calls this to perform work in a worker thread and
	      * delivers it the parameters given to AsyncTask.execute() */
	    protected String doInBackground(Integer... actions) {
	    	return admin.select(actions[0]);
	    }
	}		

	private class cmdGetInfoTask extends AsyncTask<String, Void, String> {
	    /** The system calls this to perform work in a worker thread and
	      * delivers it the parameters given to AsyncTask.execute() */
	    protected String doInBackground(String... actions) {
	    	return admin.current();
	    }
	    
	    /** The system calls this to perform work in the UI thread and delivers
	      * the result from doInBackground() */
	    protected void onPostExecute(String result) {
	    	TextView txtNowPlaying = (TextView) findViewById(R.id.txtNowPlaying);
	    	txtNowPlaying.setText(result);
	    	txtNowPlaying.setSelected(true);
	    }
	}		

	private class cmdGetStreamsTask extends AsyncTask<String, Void, String> {							
		/** The system calls this to perform work in a worker thread and
	      * delivers it the parameters given to AsyncTask.execute() */
	    protected String doInBackground(String... actions) {
	    	return admin.list();
	    }
	}		

/*****************************************************************************
 * 
 *	Scrolling text related methods
 * 
 *****************************************************************************/		
		
	/**
	 * 
	 */
	Runnable mStatusChecker = new Runnable()
	{
	     @Override 
	     public void run() {
	          updateStatus(); //this function can change value of m_interval.
	          mHandler.postDelayed(mStatusChecker, mInterval);
	     }
	};

	/**
	 * 
	 */
	private void updateStatus() {
		this.getCurrentStreamInfo();
	}
	
	/**
	 * 
	 */
	private void startRepeatingTask() {
	    this.mStatusChecker.run(); 
	}

	/**
	 * 
	 */
//	private void stopRepeatingTask() {
//	    this.mHandler.removeCallbacks(this.mStatusChecker);
//	}	
	

/*****************************************************************************
 * 
 *	Preference text related methods
 * 
 *****************************************************************************/		
	
	/**
	 * 
	 */
	public void openSettings(){
		Intent intent = new Intent(MainActivity.this, PrefActivity.class);
	    startActivity(intent);		
	}
	
	/**
	 * 
	 */
	public void loadPreferences() {
		SharedPreferences prefs = PreferenceManager
			.getDefaultSharedPreferences(this);
		 
		this.ip = prefs.getString("addr", "");
		this.port = Integer.parseInt(prefs.getString("port", ""));	 
	}

	
/*****************************************************************************
 * 
 *	dynamic dialog box
 * 
 *****************************************************************************/		

	public void doAbout() {
	
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
	
		alertDialogBuilder.setTitle(R.string.sAboutTitle);
	
		// set dialog message
		alertDialogBuilder
			.setMessage(R.string.sAppVersion)
			.setCancelable(false)
			.setNegativeButton("Ok",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					// if this button is clicked, just close
					// the dialog box and do nothing
					dialog.cancel();
				}
			});
	
			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();

			// show it
			alertDialog.show();
	}
	

// end of class
}

