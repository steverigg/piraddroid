/**
 * NetClient class
 *  Class for access remote socket server
 *  code "borrowed" from:
 *  http://stackoverflow.com/questions/5893911/android-client-socket-how-to-read-data
 */

package com.pirad.piraddroid;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class NetClient {

    /**
     * Maximum size of buffer
     */
    public static final int BUFFER_SIZE = 2048;
    private Socket socket = null;
    private PrintWriter out = null;
    private BufferedReader in = null;

    private String host = null;
    private int port = 7999;


    /**
     * Constructor with Host & Port
     * @param host
     * @param port
     */
    public NetClient(String host, int port) {
        this.host = host;
        this.port = port;
    }

	public boolean canConnect()
	{
		boolean response=false;
		Socket s = new Socket();
		try {
			s.connect(new InetSocketAddress(this.host, this.port),3000);
			response = true;
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			System.err.println("Can't connect");
			response = false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.err.println("Can't connect");
			response = false;
		}
		return response;
	}
            
    private boolean connectWithServer() {
    	boolean retVal = false;
        
    	try {
            if (this.socket == null) {
                this.socket = new Socket(this.host, this.port);
                this.out = new PrintWriter(this.socket.getOutputStream());
                this.in = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
                retVal = true;
            }
        } catch (IOException e) {
            e.printStackTrace();
            retVal = false;
        }
    	return retVal;
    }

    private void disConnectWithServer() {
        if (this.socket != null) {
            if (this.socket.isConnected()) {
                try {
                	this.in.close();
                	this.out.close();
                	this.socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void sendDataWithString(String message) {
        if (message != null) {
            if(this.connectWithServer())
            {
                this.out.write(message);
                this.out.flush();
            }
        }
    }

    public String receiveDataFromServer() {
        try {
            String message = "";
            int charsRead = 0;
            char[] buffer = new char[BUFFER_SIZE];

            while ((charsRead = this.in.read(buffer)) != -1) {
                message += new String(buffer).substring(0, charsRead);
            }

            disConnectWithServer(); // disconnect server
            return message;
        } catch (IOException e) {
            return "Error receiving response:  " + e.getMessage();
        }
    }


}